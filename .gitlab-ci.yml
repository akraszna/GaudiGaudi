#####################################################################################
# (c) Copyright 1998-2020 CERN for the benefit of the LHCb and ATLAS collaborations #
#                                                                                   #
# This software is distributed under the terms of the Apache version 2 licence,     #
# copied verbatim in the file "LICENSE".                                            #
#                                                                                   #
# In applying this licence, CERN does not waive the privileges and immunities       #
# granted to it by virtue of its status as an Intergovernmental Organization        #
# or submit itself to any jurisdiction.                                             #
#####################################################################################
image: gitlab-registry.cern.ch/lhcb-core/lbdocker/centos7-build

stages:
  - pre-build-checks
  - build
  - test

variables:
  LCG_VERSION: "100"
  CMAKE_GENERATOR: 'Ninja' # default build system
  NO_LBLOGIN: "1" # prevent lbdocker containers to start LbLogin/LbEnv
  TARGET_BRANCH: master

# All the configuration
before_script:
  # Add Ninja and CMake to the PATH
  - export PATH="/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.18.3/Linux-x86_64/bin:/cvmfs/sft.cern.ch/lcg/contrib/ninja/1.10.0/Linux-x86_64:$PATH"
  - export CCACHE_DIR=$PWD/.ccache

# Job templates
.build: &template_build
  stage: build
  tags:
    - cvmfs
  script:
    - git clone https://gitlab.cern.ch/lhcb-core/lcg-toolchains.git
    - cmake -S . -B build -DCMAKE_TOOLCHAIN_FILE=lcg-toolchains/LCG_${LCG_VERSION}/${CI_JOB_NAME}.cmake -DGAUDI_USE_INTELAMPLIFIER:BOOL=TRUE -DCMAKE_C_COMPILER_LAUNCHER=ccache -DCMAKE_CXX_COMPILER_LAUNCHER=ccache
    - ccache -z
    # pre-heat ccache cache for GaudiKernel
    - jobs=$(nproc)
    - while [[ $jobs -ge 1 ]] ; do
    -   cmake --build build -j $jobs --target GaudiKernel && break || true
    -   jobs=$(( $jobs / 2 ))
    - done
    # build from scratch
    - cmake --build build --target clean
    - cmake --build build 2>&1 | tee build/build.log
    - ccache -s
  artifacts:
    paths:
      - build
      - lcg-toolchains
    expire_in: 1 week
  cache:
    key: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
    paths:
      - .ccache

.build-check: &template_build_check
  stage: test
  script:
    - ci-utils/build-check build/build.log
  allow_failure: true

.test: &template_test
  stage: test
  tags:
    - cvmfs
  script:
    - find build -type f -exec touch -d $(date +@%s) \{} \; # not to re-run cmake
    - cd build
    - if ctest -T test -j $(nproc) --no-compress-output ; then result=success ; else result=failure ; fi
    - cp Testing/$(head -1 Testing/TAG)/Test.xml ..
    - cd ..
    - xsltproc ci-utils/CTest2JUnit.xslt Test.xml > results.xml
    - echo "Test results can be browsed at https://lhcb-nightlies.web.cern.ch/utils/test_report?url=${CI_PROJECT_URL}/-/jobs/${CI_JOB_ID}/artifacts/raw/Test.xml"
    - test "$result" = "success"
  artifacts:
    paths:
      - Test.xml
    reports:
      junit:
        - results.xml
    when: always
    expire_in: 1 week
  retry: 2


### Build
x86_64-centos7-gcc10-opt:
  <<: *template_build

x86_64-centos7-gcc10-dbg:
  <<: *template_build

view-gcc8:
  <<: *template_build
  script:
    - . /cvmfs/sft.cern.ch/lcg/views/LCG_${LCG_VERSION}/x86_64-centos7-gcc8-opt/setup.sh
    # Override CMake from the view (too old in LCG 97a)
    - export PATH="/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.18.3/Linux-x86_64/bin:$PATH"
    - PYTHON_MAJOR_VERSION=$(python -c "import sys; print(sys.version_info[0])")
    - cmake -S . -B build -DCMAKE_BUILD_TYPE=Release -DGAUDI_USE_PYTHON_MAJOR=${PYTHON_MAJOR_VERSION} -DCMAKE_C_COMPILER_LAUNCHER=ccache -DCMAKE_CXX_COMPILER_LAUNCHER=ccache
    - ccache -z
    # pre-heat ccache cache for GaudiKernel
    - jobs=$(nproc)
    - while [[ $jobs -ge 1 ]] ; do
    -   cmake --build build -j $jobs --target GaudiKernel && break || true
    -   jobs=$(( $jobs / 2 ))
    - done
    # build from scratch
    - cmake --build build --target clean
    - cmake --build build 2>&1 | tee build/build.log
    - ccache -s

lhcb-gcc10:
  <<: *template_build
  variables:
    BINARY_TAG: x86_64_v2-centos7-gcc10-opt
  script:
    - . /cvmfs/lhcb.cern.ch/lib/LbEnv
    - lb-project-init --overwrite
    - ccache -z
    - mkdir build.${BINARY_TAG}
    # pre-heat ccache cache for GaudiKernel
    - jobs=$(nproc)
    - while [[ $jobs -ge 1 ]] ; do
    -   make BUILDFLAGS=-j$jobs GaudiKernel && break || true
    -   jobs=$(( $jobs / 2 ))
    - done
    # build from scratch
    - make clean
    - make 2>&1 | tee build.${BINARY_TAG}/build.log
    - ccache -s
  artifacts:
    paths:
      - build.${BINARY_TAG}
    expire_in: 1 week

### Tests
x86_64-centos7-gcc10-opt:test:
  <<: *template_test
  needs:
    - job: "x86_64-centos7-gcc10-opt"
      artifacts: true

x86_64-centos7-gcc10-dbg:test:
  <<: *template_test
  needs:
    - job: "x86_64-centos7-gcc10-dbg"
      artifacts: true

view-gcc8:test:
  <<: *template_test
  needs:
    - job: "view-gcc8"
      artifacts: true
  script:
    - . /cvmfs/sft.cern.ch/lcg/views/LCG_${LCG_VERSION}/x86_64-centos7-gcc8-opt/setup.sh
    # Override CMake from the view (too old in LCG 97a)
    - export PATH="/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.18.3/Linux-x86_64/bin:$PATH"
    - find build -type f -exec touch -d $(date +@%s) \{} \; # not to re-run cmake
    - cd build
    - if ctest -T test -j 4 --no-compress-output ; then result=success ; else result=failure ; fi
    - cp Testing/$(head -1 Testing/TAG)/Test.xml ..
    - cd ..
    - xsltproc ci-utils/CTest2JUnit.xslt Test.xml > results.xml
    - echo "Test results can be browsed at https://lhcb-nightlies.web.cern.ch/utils/test_report?url=${CI_PROJECT_URL}/-/jobs/${CI_JOB_ID}/artifacts/raw/Test.xml"
    - test "$result" = "success"

lhcb-gcc10:test:
  <<: *template_test
  needs:
    - job: "lhcb-gcc10"
      artifacts: true
  variables:
    BINARY_TAG: x86_64_v2-centos7-gcc10-opt
  script:
    - . /cvmfs/lhcb.cern.ch/lib/LbEnv
    - lb-project-init --overwrite
    - find build.${BINARY_TAG} -type f -exec touch -d $(date +@%s) \{} \; # not to re-run cmake
    - if make test ARGS="-j 4 --no-compress-output" ; then result=success ; else result=failure ; fi
    - cp build.${BINARY_TAG}/Testing/$(head -1 build.${BINARY_TAG}/Testing/TAG)/Test.xml .
    - mv build.${BINARY_TAG}/html .
    - xsltproc ci-utils/CTest2JUnit.xslt Test.xml > results.xml
    - echo "Test results can be browsed at https://lhcb-nightlies.web.cern.ch/utils/test_report?url=${CI_PROJECT_URL}/-/jobs/${CI_JOB_ID}/artifacts/raw/Test.xml"
    - test "$result" = "success"
  artifacts:
    paths:
      - Test.xml
      - html
    reports:
      junit:
        - results.xml
    when: always
    expire_in: 1 day

### Check build outputs
x86_64-centos7-gcc10-opt:build-check:
  <<: *template_build_check
  needs:
    - job: "x86_64-centos7-gcc10-opt"
      artifacts: true

x86_64-centos7-gcc10-dbg:build-check:
  <<: *template_build_check
  needs:
    - job: "x86_64-centos7-gcc10-dbg"
      artifacts: true

view-gcc8:build-check:
  <<: *template_build_check
  needs:
    - job: "view-gcc8"
      artifacts: true

lhcb-gcc10:build-check:
  <<: *template_build_check
  needs:
    - job: "lhcb-gcc10"
      artifacts: true
  variables:
    BINARY_TAG: x86_64_v2-centos7-gcc10-opt
  script:
    - ci-utils/build-check build.${BINARY_TAG}/build.log

### Misc checks
x86_64-centos7-gcc10-opt:check-unused:
  stage: test
  needs:
    - job: "x86_64-centos7-gcc10-opt"
      artifacts: true
  script:
    - ci-utils/check-sources build/build.ninja
  allow_failure: true

check-formatting:
  stage: pre-build-checks
  image: gitlab-registry.cern.ch/gaudi/gaudi/format-checker:latest
  script:
    - ci-utils/check-formatting
  artifacts:
    paths:
      - apply-formatting.patch
    when: on_failure
    expire_in: 1 week
  allow_failure: true

check-copyright:
  stage: pre-build-checks
  image: gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
  script:
    - curl -o lb-check-copyright "https://gitlab.cern.ch/lhcb-core/LbDevTools/-/raw/master/LbDevTools/SourceTools.py?inline=false"
    - python lb-check-copyright --exclude lhcbproject.yml origin/${TARGET_BRANCH}
  allow_failure: true
